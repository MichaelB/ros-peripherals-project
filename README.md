ROS Peripherals Project
Contains ROS plugins and supporting programs.

The purpose is to create basic intergration of multiple types of peripherals in order to archive an immersive mutual path of communication between the user and the simulated robot. The user is then to aid the robot in perfoming its tasks.

Requirements:
- ROS Jade (includes)
- Catkin (and a workspace already set up)
- Gazebo
- Python 3.0

Instructions:

1. Clone the repositary into the source folder of your catkin workspace. Eg: "~/catkin_ws/src/"
2. Within the root folder of the catkin workspace run "catkin_make". Everything should pass when using the production branch.
3. Open a terminal and run "roscore"
4. Open another terminal and run "roslaunch tank_gazebo tank.launch" This will open gazebo and launch the plugin.
5. If you have the leap motion device connected go to 5. a) otherwise go to 5. b) I would advice opening a new terminal and cd into *CATKIN_WORKSPACE_ROOT_FOLDER*/src/ros-peripherals-project/tank_pkgs/pyleap_publisher/
5. a) Run the python script leaptalker.py eg: "python leaptalker.py"
5. b) Run the python script leaptalker.py but using one of the recorded data sets. eg: "python leaptalker.py -i sample/medleap2handsample.txt"

You should now be able to send input using the leap motion or view simulated input within the robot in Gazebo.
Note leaptalker.py useage: 
leaptalker.py -i *INSERT SIMULATED LEAP INPUT* 
leaptalker.py -o *INSERT FILE TO RECORD LEAP DATA FOR LATER SIMULATION*
