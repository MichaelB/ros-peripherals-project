
#include <string>
#include <gazebo/gazebo.hh>
#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include <gazebo/common/common.hh>
#include <ros/ros.h>
#include <boost/bind.hpp>
#include <stdio.h>

namespace gazebo
{

  class TankPlugin : public ModelPlugin{


	public: void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf){

	    if (!ros::isInitialized()){
	      ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
		<< "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
	      return;
	    }

	    ROS_INFO("Now loading the Tank Gazebo plugin!");

	    this->model = _model;
	    this->sdf = _sdf;

	    this->prevUpdateTime = common::Time::Zero;
	    this->maxTimeBeforeToggle = 2.0; //in seconds
	    this->counter = 0.0;
	    this->turning = true;

	    //Init wheel joints
	    std::string tankTopic = "~/tank";
	    if(this->sdf->HasElement("topic"))
		tankTopic = this->sdf->Get<std::string>("topic");

	    std::string jointName  = this->sdf->Get<std::string>("left_front_wheel");
	    this->leftFrontWheel = this->model->GetJoint(jointName);

	    jointName  = this->sdf->Get<std::string>("right_front_wheel");
	    this->rightFrontWheel = this->model->GetJoint(jointName);

	    jointName  = this->sdf->Get<std::string>("left_back_wheel");
	    this->leftBackWheel = this->model->GetJoint(jointName);

	    jointName  = this->sdf->Get<std::string>("right_back_wheel");
	    this->rightBackWheel = this->model->GetJoint(jointName);

	    if(!this->leftFrontWheel || !this->rightFrontWheel ||
		    !this->leftBackWheel || !this->rightBackWheel){
		gzerr << "Unable to find a wheel joint.\n";
		gzerr << "The tank plugin is disabled.\n";
		return;
	    }

	    this->node = transport::NodePtr(new transport::Node());
	    this->node->Init(this->model->GetWorld()->GetName());

	    // Subscribe to the tank topic.
	    this->tankSub = this->node->Subscribe(tankTopic,
		  &TankPlugin::OnMessage, this);

	    //Start update connections
	    this->updateConnection = event::Events::ConnectWorldUpdateBegin(
		        boost::bind(&TankPlugin::OnUpdate,this,_1));


	}

/////////////////////////////////////////////////

	public: void OnUpdate(const common::UpdateInfo & _info)
	{
	    //Init the time
	    if (this->prevUpdateTime == common::Time::Zero)
	    {
	      this->prevUpdateTime = _info.simTime;
	      return;
	    }

	    this->counter += _info.simTime.Double() - prevUpdateTime.Double();

	    if(this->counter >= this->maxTimeBeforeToggle){
		this->counter = 0.0;
		this->turning = !this->turning;
	    }

	    //Set a wheel force
	    double wheelForce = 1000.0;
	    if(this->turning)
		wheelForce = 1000.0;

	    this->leftFrontWheel->SetForce(0,-wheelForce);

	    this->rightFrontWheel->SetForce(0,wheelForce);

	    this->leftBackWheel->SetForce(0,-wheelForce);

	    this->rightBackWheel->SetForce(0,wheelForce);

	    this->prevUpdateTime = _info.simTime;
	}


    private: void OnMessage(ConstGzStringPtr &_msg);
    private: physics::ModelPtr model;
    private: sdf::ElementPtr sdf;
    private: physics::JointPtr leftFrontWheel;
    private: physics::JointPtr rightFrontWheel;
    private: physics::JointPtr leftBackWheel;
    private: physics::JointPtr rightBackWheel;
    private: event::ConnectionPtr updateConnection;
    private: transport::SubscriberPtr tankSub;
    private: transport::NodePtr node;
    private: common::Time prevUpdateTime;
    private: double maxTimeBeforeToggle;
    private: double counter;
    private: bool turning;
  };

  GZ_REGISTER_MODEL_PLUGIN(TankPlugin)
}

