#include <gazebo/gazebo.hh>
#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include <gazebo/common/common.hh>
#include <ros/ros.h>
#include "std_msgs/String.h"
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>
#include <stdio.h>

namespace gazebo
{

class TankPlugin : public ModelPlugin{

private: physics::ModelPtr model;
private: sdf::ElementPtr sdf;
private: physics::JointPtr leftFrontWheel;
private: physics::JointPtr rightFrontWheel;
private: physics::JointPtr leftBackWheel;
private: physics::JointPtr rightBackWheel;
private: physics::JointPtr turretJoint;
private: event::ConnectionPtr updateConnection;
private: ros::Subscriber tankSub;
private: ros::NodeHandle node;
private: ros::Publisher chatter_pub;
private: common::Time prevUpdateTime;
private: int num_hands;
private: double turretRotVel;
private: double maxTimeBeforeToggle;
private: double counter;
private: bool turning;


private: void callback(const std_msgs::String::ConstPtr& msg){
	std::string data = msg->data.c_str();
	std_msgs::String out;
	int tmploc = data.find("hands:");
	//check to see if message is somewhat valid having the tag 'hands'
	std::stringstream ss;

	if(tmploc>0){
		//grab it's value and parse to int
		std::string tmpsubstr = data.substr(tmploc+6,1);

		num_hands = atoi(tmpsubstr.c_str());
		//ROS_INFO("Num Hands: [%s]",tmpsubstr.c_str());
		ss << "Num Hands: [" << tmpsubstr << "]";
		//I only care about 1 or 2 hands
		if(num_hands == 1 || num_hands == 2){
			//only want right hand
			tmploc = data.find("hand:right");
			if(tmploc > 0){
				//get right hand index finger
				tmpsubstr = data.substr(tmploc+10);
				tmploc = tmpsubstr.find("finger:index");
				if(tmploc > 0){
					//get distal bones (tip of finger)
					tmpsubstr = tmpsubstr.substr(tmploc+12);
					tmploc = tmpsubstr.find("bone:distal");
					if(tmploc > 0){
						tmpsubstr = tmpsubstr.substr(tmploc+11);
						//now get bone end x position
						tmploc = tmpsubstr.find("end:");
						if(tmploc > 0){
							//tmploc+5 because we don't want the '('
							tmpsubstr = tmpsubstr.substr(tmploc+5);
							tmploc = tmpsubstr.find(",");
							if(tmploc > 0){
								//cut off the data we don't need
								tmpsubstr = tmpsubstr.substr(0,tmploc);
								//now tmpsubstr should now just contain a double
								ss << "Right Index Finger X pos [" << tmpsubstr << "]";
								turretRotVel = atof(tmpsubstr.c_str());
							}
						}

					}
				}
			}
		}
	}

	out.data = ss.str();
	publish(out);
}

private: void publish(std_msgs::String msg){
	ROS_INFO("%s", msg.data.c_str());
	chatter_pub.publish(msg);
}

public: void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf){

	if (!ros::isInitialized()){
		ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
				<< "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
		return;
	}

	ROS_INFO("Now loading the Tank Gazebo plugin!");

	this->model = _model;
	this->sdf = _sdf;

	this->prevUpdateTime = common::Time::Zero;
	this->maxTimeBeforeToggle = 2.0; //in seconds
	this->counter = 0.0;
	this->turning = true;

	//Init wheel joints
	std::string leapTopic = "/leapchatter";
	if(this->sdf->HasElement("topic"))
		leapTopic = this->sdf->Get<std::string>("topic");

	std::string jointName  = this->sdf->Get<std::string>("left_front_wheel");
	this->leftFrontWheel = this->model->GetJoint(jointName);

	jointName  = this->sdf->Get<std::string>("right_front_wheel");
	this->rightFrontWheel = this->model->GetJoint(jointName);

	jointName  = this->sdf->Get<std::string>("left_back_wheel");
	this->leftBackWheel = this->model->GetJoint(jointName);

	jointName  = this->sdf->Get<std::string>("right_back_wheel");
	this->rightBackWheel = this->model->GetJoint(jointName);

	jointName  = this->sdf->Get<std::string>("turret_joint");
	this->turretJoint = this->model->GetJoint(jointName);

	if(!this->leftFrontWheel || !this->rightFrontWheel ||
			!this->leftBackWheel || !this->rightBackWheel){
		gzerr << "Unable to find a wheel joint.\n";
		gzerr << "The tank plugin is disabled.\n";
		return;
	}

	// Subscribe to the tank topic.
	tankSub = node.subscribe(leapTopic,1000,&TankPlugin::callback,this);

	//Start update connections
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
			boost::bind(&TankPlugin::onUpdate,this,_1));

	chatter_pub = node.advertise<std_msgs::String>("tankinfo", 1000);

	ROS_INFO("Finished loading tank plugin");
}

public: void onUpdate(const common::UpdateInfo & _info){
	//Init the time
	if (this->prevUpdateTime == common::Time::Zero){
		this->prevUpdateTime = _info.simTime;
		return;
	}

	this->counter += _info.simTime.Double() - prevUpdateTime.Double();

	if(this->counter >= this->maxTimeBeforeToggle){
		this->counter = 0.0;
		this->turning = !this->turning;
	}

	//Set a wheel force
	double wheelForce = 5000.0;
	if(this->turning)
		wheelForce = 5000.0;

	this->turretJoint->SetForce(0,-this->turretRotVel);

	this->leftFrontWheel->SetForce(0,-wheelForce);

	this->rightFrontWheel->SetForce(0,wheelForce);

	this->leftBackWheel->SetForce(0,-wheelForce);

	this->rightBackWheel->SetForce(0,wheelForce);

	this->prevUpdateTime = _info.simTime;

	ros::spinOnce();
}





};

GZ_REGISTER_MODEL_PLUGIN(TankPlugin)
}

