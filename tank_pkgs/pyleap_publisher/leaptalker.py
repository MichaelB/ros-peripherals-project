#!/usr/bin/env python

import rospy
import argparse
from std_msgs.msg import String
import Leap, sys, thread, time

finger_names = ['thumb', 'index', 'middle', 'ring', 'pinky']
bone_names = ['metacarpal', 'proximal', 'intermediate', 'distal']
state_names = ['STATE_INVALID', 'STATE_START', 'STATE_UPDATE', 'STATE_END']
outinfo = []

def talker():
    pub = rospy.Publisher('leapchatter', String, queue_size=10)
    rospy.init_node('leaptalker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    
    def send_info(out_str):
        rospy.loginfo(out_str+"\n")
        pub.publish(out_str)
        outinfo.append(out_str)

    if in_file:
	#publish sample data
	with open(in_file,'r',1) as input_file:
	    for line in input_file:
	    	out_str = line
	    	send_info(out_str)
	    	rate.sleep()
    else:
	#publish actual data
	controller = Leap.Controller()
	while not rospy.is_shutdown():
	    out_str = on_frame(controller.frame(0))
	    send_info(out_str)
	    rate.sleep()
	if out_file:
	    #write recorded data
	    with open(out_file,'w') as output:
	        for text in outinfo:
		    output.write(text+"\n")

def on_frame(frame):
        # Get the most recent frame and report some information
	out = "frameid:%d-timestamp:%d-hands:%d-fingers:%d-" % (
              frame.id, frame.timestamp, len(frame.hands), len(frame.fingers))

        # Get hands
        for hand in frame.hands:

            handType = "left" if hand.is_left else "right"

            out+="hand:%s-id:%d-position:%s-" % (
                handType, hand.id, hand.palm_position)

            # Get the hand's normal vector and direction
            normal = hand.palm_normal
            direction = hand.direction

            # Calculate the hand's pitch, roll, and yaw angles
            out+="pitch:%f-roll:%f-yaw:%f-" % (
                direction.pitch * Leap.RAD_TO_DEG,
                normal.roll * Leap.RAD_TO_DEG,
                direction.yaw * Leap.RAD_TO_DEG)

            # Get arm bone
            arm = hand.arm
            out+="armdirection:%s-wristposition:%s-elbowposition:%s-" % (
                arm.direction,
                arm.wrist_position,
                arm.elbow_position)

            # Get fingers
            for finger in hand.fingers:

                out+="finger:%s-id:%d-length:%f-width:%f-" % (
                    finger_names[finger.type],
                    finger.id,
                    finger.length,
                    finger.width)

                # Get bones
                for b in range(0, 4):
                    bone = finger.bone(b)
                    out+="bone:%s-start:%s-end:%s-direction:%s" % (
                        bone_names[bone.type],
                        bone.prev_joint,
                        bone.next_joint,
                        bone.direction)

	return out;

if __name__ == '__main__':
    try:
	#define help page and input parameters
	# -i for file to read as input (and post it's output)
	# -o for file to save recording for later use
	parser = argparse.ArgumentParser(description='Grab data from Leap motion or Sample output and publish to ROS. Requires an instance on ROS core running. \n Note: declaring to read a file and write to one will result in only reading. Output will be ignored.')
        parser.add_argument('-i',metavar='Input',type=str,help='filename of sample output')
        parser.add_argument('-o',metavar='Output',type=str,help='filename of file to store data')
        args = parser.parse_args()
	in_file = args.i
        out_file = args.o
        talker()
    except rospy.ROSInterruptException:
        pass
