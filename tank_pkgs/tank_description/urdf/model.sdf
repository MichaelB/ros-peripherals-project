<?xml version='1.0'?>
<sdf version="1.4">
<model name="Robot_Tank">
  <pose>0 0 0 0 0 0</pose>
  <static>false</static>

  <link name="base"> <!-- Pose 0,0,1.1 -->
      <gravity>true</gravity>
      <pose>0 0 1.1 0 0 0</pose>
      <must_be_base_link>true</must_be_base_link>
      <inertial>
        <mass>2.5</mass>
        <inertia>
          <ixx>3.7869</ixx>       <!-- for a box: ixx = 0.083 * 2.5 * (16 + 2.25) -->
          <ixy>0.0</ixy>         <!-- for a box: ixy = 0 -->
          <ixz>0.0</ixz>         <!-- for a box: ixz = 0 -->
	  <iyy>1.662</iyy>       <!-- for a box: iyy = 0.083 * 2.5 * (5.76 + 2.25) -->
          <iyz>0.0</iyz>         <!-- for a box: iyz = 0 -->
	  <izz>4.5152</izz>       <!-- for a box: izz = 0.083 * 2.5 * (5.76 + 16) -->
        </inertia>
      </inertial>
      <collision name="collision">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/collisionbase.stl</uri>
          </mesh>
        </geometry>
      </collision>
      <visual name="visual">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/base.stl</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <uri>model://tank/materials/scripts</uri>
            <uri>model://tank/materials/textures</uri>
            <name>Tank/Tank_Texture</name>
          </script>
        </material>
      </visual>
  </link>

  <link name="top"> <!-- Pose 0,0.5,2.1 -->
      <gravity>true</gravity>
      <pose>0 0.5 2.1 0 0 0</pose>
      <inertial>
        <mass>1.0</mass>
        <inertia>
          <ixx>0.292</ixx>       <!-- for a ellipse: ixx = 1/5 * 1.0 * (1.21 + 0.25) -->
          <ixy>0.0</ixy>         <!-- for a ellipse: ixy = 0 -->
          <ixz>0.0</ixz>         <!-- for a ellipse: ixz = 0 -->
          <iyy>0.292</iyy>       <!-- for a ellipse: iyy = 1/5 * 1.0 * (1.21 + 0.25) -->
          <iyz>0.0</iyz>         <!-- for a ellipse: iyz = 0 -->
          <izz>0.484</izz>       <!-- for a ellipse: izz = 1/5 * 1.0 * (1.21 + 1.21) -->
        </inertia>
      </inertial>
      <collision name="collision">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/collisiontop.stl</uri>
          </mesh>
        </geometry>
      </collision>
      <visual name="visual">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/top.stl</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <uri>model://tank/materials/scripts</uri>
            <uri>model://tank/materials/textures</uri>
            <name>Tank/Tank_Texture</name>
          </script>
        </material>
      </visual>

    <sensor name="laser_sensor" type="depth">
      <pose>0 -1.5 0 0 0 0</pose>
      <always_on>true</always_on>
      <update_rate>30</update_rate>
      <visualize>true</visualize>
      <camera>
          <horizontal_fov>1.047198</horizontal_fov>
          <image>
            <width>640</width>
            <height>480</height>
            <format>R8G8B8</format>
          </image>
          <clip>
            <near>1</near>
            <far>10</far>
          </clip>
        </camera>
    </sensor>

  </link>

  <link name="front_left"> <!-- Pose 1.4,-1.24,0.4 -->
      <gravity>true</gravity>
      <pose>1.4 -1.24 0.4 0 0 0</pose>
      <inertial>
        <mass>0.5</mass>
        <inertia>
          <ixx>0.0726</ixx>       <!-- for a cyl: ixx = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <ixy>0.0</ixy>         <!-- for a cyl: ixy = 0 -->
          <ixz>0.0</ixz>         <!-- for a cyl: ixz = 0 -->
          <iyy>0.0726</iyy>       <!-- for a cyl: iyy = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <iyz>0.0</iyz>         <!-- for a cyl: iyz = 0 -->
          <izz>0.375</izz>       <!-- for a cyl: izz = 0.5 * 0.5 * 3*0.5 -->
        </inertia>
      </inertial>
      <collision name="collision">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/collisionwheel.stl</uri>
          </mesh>
        </geometry>
      </collision>
      <visual name="visual">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/wheel.stl</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <uri>model://tank/materials/scripts</uri>
            <uri>model://tank/materials/textures</uri>
            <name>Tank/Tank_Texture</name>
          </script>
        </material>
      </visual>
  </link>

  <link name="front_right"> <!-- Pose -1.4,-1.24,0.4 -->
      <gravity>true</gravity>
      <pose>-1.4 -1.24 0.4 0 0 0</pose>
      <inertial>
        <mass>0.5</mass>
        <inertia>
          <ixx>0.0726</ixx>       <!-- for a cyl: ixx = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <ixy>0.0</ixy>         <!-- for a cyl: ixy = 0 -->
          <ixz>0.0</ixz>         <!-- for a cyl: ixz = 0 -->
          <iyy>0.0726</iyy>       <!-- for a cyl: iyy = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <iyz>0.0</iyz>         <!-- for a cyl: iyz = 0 -->
          <izz>0.375</izz>       <!-- for a cyl: izz = 0.5 * 0.5 * 3*0.5 -->
        </inertia>
      </inertial>
      <collision name="collision">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/collisionwheel.stl</uri>
          </mesh>
        </geometry>
      </collision>
      <visual name="visual">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/wheel.stl</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <uri>model://tank/materials/scripts</uri>
            <uri>model://tank/materials/textures</uri>
            <name>Tank/Tank_Texture</name>
          </script>
        </material>
      </visual>
  </link>

  <link name="back_left"> <!-- Pose 1.4,1.14,0.4 -->
      <gravity>true</gravity>
      <pose>1.4 1.14 0.4 0 0 0</pose>
      <inertial>
        <mass>0.5</mass>
        <inertia>
          <ixx>0.0726</ixx>       <!-- for a cyl: ixx = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <ixy>0.0</ixy>         <!-- for a cyl: ixy = 0 -->
          <ixz>0.0</ixz>         <!-- for a cyl: ixz = 0 -->
          <iyy>0.0726</iyy>       <!-- for a cyl: iyy = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <iyz>0.0</iyz>         <!-- for a cyl: iyz = 0 -->
          <izz>0.375</izz>       <!-- for a cyl: izz = 0.5 * 0.5 * 3*0.5 -->
        </inertia>
      </inertial>
      <collision name="collision">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/collisionwheel.stl</uri>
          </mesh>
        </geometry>
      </collision>
      <visual name="visual">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/wheel.stl</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <uri>model://tank/materials/scripts</uri>
            <uri>model://tank/materials/textures</uri>
            <name>Tank/Tank_Texture</name>
          </script>
        </material>
      </visual>
  </link>

  <link name="back_right"> <!-- Pose -1.4,1.14,0.4 -->
      <gravity>true</gravity>
      <pose>-1.4 1.14 0.4 0 0 0</pose>
      <inertial>
        <mass>0.5</mass>
        <inertia>
          <ixx>0.0726</ixx>       <!-- for a cyl: ixx = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <ixy>0.0</ixy>         <!-- for a cyl: ixy = 0 -->
          <ixz>0.0</ixz>         <!-- for a cyl: ixz = 0 -->
          <iyy>0.0726</iyy>       <!-- for a cyl: iyy = 0.083 * 0.5 * (3*0.5 + 0.25) -->
          <iyz>0.0</iyz>         <!-- for a cyl: iyz = 0 -->
          <izz>0.375</izz>       <!-- for a cyl: izz = 0.5 * 0.5 * 3*0.5 -->
        </inertia>
      </inertial>
      <collision name="collision">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/collisionwheel.stl</uri>
          </mesh>
        </geometry>
      </collision>
      <visual name="visual">
        <geometry>
          <mesh>
            <uri>model://tank/meshes/wheel.stl</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <uri>model://tank/materials/scripts</uri>
            <uri>model://tank/materials/textures</uri>
            <name>Tank/Tank_Texture</name>
          </script>
        </material>
      </visual>
  </link>

  <joint name="top_base_joint" type="revolute">
    <parent>base</parent>
    <child>top</child>
    <axis>
      <xyz>0 0 1</xyz>
    </axis>

   
  </joint>

  <joint name="front_left_base_joint" type="revolute">
    <parent>base</parent>
    <child>front_left</child>
    <axis>
      <xyz>1 0 0</xyz>
    </axis>
  </joint>

  <joint name="front_right_base_joint" type="revolute">
    <parent>base</parent>
    <child>front_right</child>
    <axis>
      <xyz>1 0 0</xyz>
    </axis>
  </joint>

  <joint name="back_left_base_joint" type="revolute">
    <parent>base</parent>
    <child>back_left</child>
    <axis>
      <xyz>1 0 0</xyz>
    </axis>
  </joint>

  <joint name="back_right_base_joint" type="revolute">
    <parent>base</parent>
    <child>back_right</child>
    <axis>
      <xyz>1 0 0</xyz>
    </axis>
  </joint>

  <plugin filename="libtankplugin.so" name="tank_plugin">
        <left_front_wheel>Robot_Tank::front_left_base_joint</left_front_wheel>
	<right_front_wheel>Robot_Tank::front_right_base_joint</right_front_wheel>
	<left_back_wheel>Robot_Tank::back_left_base_joint</left_back_wheel>
	<right_back_wheel>Robot_Tank::back_right_base_joint</right_back_wheel>

        <topic>~/tank</topic>
      </plugin>

</model>
</sdf>
