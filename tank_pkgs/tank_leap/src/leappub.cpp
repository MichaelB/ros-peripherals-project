#include "std_msgs/String.h"
#include <stdio.h>
#include "ros/ros.h"
#include <sstream>
#include "Leap.h"

using namespace Leap;

const std::string fingerNames[] = {"Thumb", "Index", "Middle", "Ring", "Pinky"};
const std::string boneNames[] = {"Metacarpal", "Proximal", "Middle", "Distal"};
const std::string stateNames[] = {"STATE_INVALID", "STATE_START", "STATE_UPDATE", "STATE_END"};


int main(int argc, char **argv)
{
	ros::init(argc, argv, "leaptalker");

	ros::NodeHandle n;

	ros::Publisher chatter_pub = n.advertise<std_msgs::String>("leapinfo", 1000);

	ros::Rate loop_rate(10);

	Controller controller;


	if (argc > 1 && strcmp(argv[1], "--bg") == 0)
		controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);

	int count = 0;
	while (ros::ok())
	{
		std_msgs::String msg;

		std::stringstream ss;
		ss << onFrame(controller.frame()) << count;
		msg.data = ss.str();

		ROS_INFO("%s", msg.data.c_str());

		chatter_pub.publish(msg);

		ros::spinOnce();

		loop_rate.sleep();
		++count;
	}


	return 0;
}



std::string onFrame(const Frame frame) {

	std::string out;

	// Get the most recent frame and report some basic information
	out << "Frame id: " << frame.id()
            		<< ", timestamp: " << frame.timestamp()
					<< ", hands: " << frame.hands().count()
					<< ", extended fingers: " << frame.fingers().extended().count()
					<< ", tools: " << frame.tools().count()
					<< ", gestures: " << frame.gestures().count();
	std::cout << out << std::endl;

	HandList hands = frame.hands();
	for (HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
		// Get the first hand
		const Hand hand = *hl;
		std::string handType = hand.isLeft() ? "Left hand" : "Right hand";
		out << std::string(2, ' ') << handType << ", id: " << hand.id()
            		  << ", palm position: " << hand.palmPosition();
		std::cout << out << std::endl;

		// Get the hand's normal vector and direction
		const Vector normal = hand.palmNormal();
		const Vector direction = hand.direction();

		// Calculate the hand's pitch, roll, and yaw angles
		out << std::string(2, ' ') <<  "pitch: " << direction.pitch() * RAD_TO_DEG << " degrees, "
				<< "roll: " << normal.roll() * RAD_TO_DEG << " degrees, "
				<< "yaw: " << direction.yaw() * RAD_TO_DEG << " degrees";
		std::cout << out << std::endl;

		// Get the Arm bone
		Arm arm = hand.arm();
		out << std::string(2, ' ') <<  "Arm direction: " << arm.direction()
            		  << " wrist position: " << arm.wristPosition()
					  << " elbow position: " << arm.elbowPosition();
		std::cout << out << std::endl;

		// Get fingers
		const FingerList fingers = hand.fingers();
		for (FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl) {
			const Finger finger = *fl;
			out << std::string(4, ' ') <<  fingerNames[finger.type()]
															 << " finger, id: " << finger.id()
															 << ", length: " << finger.length()
															 << "mm, width: " << finger.width();
			std::cout << out << std::endl;

			// Get finger bones
			for (int b = 0; b < 4; ++b) {
				Bone::Type boneType = static_cast<Bone::Type>(b);
				Bone bone = finger.bone(boneType);
				out << std::string(6, ' ') <<  boneNames[boneType]
															   << " bone, start: " << bone.prevJoint()
															   << ", end: " << bone.nextJoint()
															   << ", direction: " << bone.direction();
				std::cout << out << std::endl;
			}
		}
	}

	if (!frame.hands().isEmpty()) {
		std::cout << std::endl;
	}

	return out;

}


